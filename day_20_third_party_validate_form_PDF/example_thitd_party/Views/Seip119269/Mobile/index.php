<a href="create.php"> Add Model</a>

<?php
include ('../../../vendor/autoload.php');

use App\Seip119269\Mobile\Mobile;
    $myobj = new Mobile();
    $items = $myobj->index();

    if(isset($_SESSION['Message'])&& !empty($_SESSION['Message'])){
        echo $_SESSION['Message'];
        unset($_SESSION['Message']);
    }
?>

<table border="1">
    <tr>
        <th>ID</th>
        <th>Title</th>
        <th colspan="2">Download</th>
        <th colspan="3">Action</th>
    </tr>
    <?php 
    if(isset($items)){
        foreach ($items as $item) { ?>
        <tr>
            <td><?php echo $item['id'];?></td>
            <td><?php echo $item['title'];?></td>
            <td><a href="pdf.php"> PDF </a></td>
            <td><a href="xl.php">Exal</a></td>
            <td><a href="show.php?id=<?php echo $item['u_id'];?>"> View </a></td>
            <td><a href="edit.php?id=<?php echo $item['u_id'];?>" onclick="return confirm('Are you sure you want to Edit?');">Edit</a></td>
            <td><a href="delete.php?id=<?php echo $item['u_id'];?>" onclick="return confirm('Are you sure you want to Delete?');">Delete</a></td>
        </tr>
    <?php }}?>
</table>