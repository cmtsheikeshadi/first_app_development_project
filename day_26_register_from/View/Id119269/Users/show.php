<?php
include_once ('../../../vendor/autoload.php');

use App\Id119269\Car\Car;

$obj = new Car();
$obj->prepare($_GET);
$data = $obj->show();
//print_r($data);      
?>
<a href="index.php">Go to back List page</a>
<table border="1">
    <tr>
        <th>id</th>
        <th>Car Name</th>
        <th>Car Color</th>
    </tr>
    <tr>
        <td><?php echo $data['id'];?></td>
        <td><?php echo $data['c_name'];?></td>
        <td><?php echo $data['c_color'];?></td>
    </tr>
</table>