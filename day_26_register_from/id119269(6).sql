-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 21, 2016 at 01:09 PM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `id119269`
--

-- --------------------------------------------------------

--
-- Table structure for table `books`
--

CREATE TABLE IF NOT EXISTS `books` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `books`
--

INSERT INTO `books` (`id`, `title`) VALUES
(1, 'Bangla Book'),
(2, 'English Book'),
(3, 'Math book'),
(4, 'Social book'),
(5, 'Islam book');

-- --------------------------------------------------------

--
-- Table structure for table `check`
--

CREATE TABLE IF NOT EXISTS `check` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `check` varchar(255) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `check`
--

INSERT INTO `check` (`id`, `title`, `check`) VALUES
(11, 'Shadi', 'Check'),
(12, 'Mustain', ''),
(13, 'Titu', 'Check'),
(15, 'Shadi', 'Check');

-- --------------------------------------------------------

--
-- Table structure for table `exam`
--

CREATE TABLE IF NOT EXISTS `exam` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `semester` varchar(255) NOT NULL,
  `check` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `gender`
--

CREATE TABLE IF NOT EXISTS `gender` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `gender` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gender`
--

INSERT INTO `gender` (`id`, `title`, `gender`) VALUES
(7, 'Shadi', 'male');

-- --------------------------------------------------------

--
-- Table structure for table `mobile`
--

CREATE TABLE IF NOT EXISTS `mobile` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mobile`
--

INSERT INTO `mobile` (`id`, `title`) VALUES
(25, 'HTC'),
(24, 'Symphony'),
(23, 'Vision'),
(22, 'Walton'),
(20, 'LG'),
(16, 'Dell'),
(15, 'Apple'),
(21, 'Sony');

-- --------------------------------------------------------

--
-- Table structure for table `mobile_2`
--

CREATE TABLE IF NOT EXISTS `mobile_2` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `mobile` varchar(255) NOT NULL,
  `is_delete` int(11) NOT NULL,
  `time` varchar(255) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mobile_2`
--

INSERT INTO `mobile_2` (`id`, `name`, `mobile`, `is_delete`, `time`) VALUES
(16, 'A', '01932371741', 0, '11:26:50am'),
(14, 'Shojib', '54554', 1, '12:24:32pm'),
(20, 'Shadi', '', 0, '12:37:56pm'),
(12, 'Liton', '0211', 1, '12:06:34pm'),
(19, 'dasd432432', '01932371741', 0, '11:43:58am'),
(11, 'Shadi', '01932371741', 1, '12:05:55pm'),
(15, 'BITMS''s', '01932371741', 0, '12:06:51pm'),
(22, 'Shadi', '', 0, '12:44:34pm'),
(21, 'Shadi', '', 0, '12:43:49pm'),
(23, 'BITMS''s', '', 0, '12:44:40pm'),
(24, 'Shadi', '', 0, '12:44:44pm'),
(25, 'Mustain', '', 0, '12:44:49pm'),
(26, 'dasd432432', '', 0, '12:44:54pm'),
(27, 'dasd432432', '', 0, '12:44:59pm'),
(28, 'BITMS''s', '', 0, '12:45:12pm'),
(29, 'BITMS''s', '01932371741', 0, '01:12:19pm');

-- --------------------------------------------------------

--
-- Table structure for table `pdo`
--

CREATE TABLE IF NOT EXISTS `pdo` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `semester` varchar(255) NOT NULL,
  `tik` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pdo`
--

INSERT INTO `pdo` (`id`, `name`, `semester`, `tik`) VALUES
(2, 'Corola dffds', 'third', 'tik'),
(5, 'Apple', 'second', 'tik'),
(6, 'Orange fd', 'first', ''),
(12, 'Shadi', 'second', 'tik'),
(13, 'Shadi', 'second', 'tik'),
(14, 'BITMSs', 'first', 'tik');

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE IF NOT EXISTS `student` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `s` varchar(255) NOT NULL,
  `c` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`id`, `name`, `s`, `c`) VALUES
(10, 'Shadi', 'Semester 1', 'Yes');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `terms` varchar(255) NOT NULL,
  `unique_id` varchar(255) NOT NULL,
  `is_active` varchar(255) NOT NULL,
  `is_admin` int(11) NOT NULL,
  `created_at` tinyint(4) NOT NULL,
  `deleted_at` tinyint(4) NOT NULL,
  `is_delete` tinyint(4) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=41 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `user_name`, `email`, `password`, `terms`, `unique_id`, `is_active`, `is_admin`, `created_at`, `deleted_at`, `is_delete`) VALUES
(39, 'Liton', 'liton@gmail.com', '1', 'Yes', '5790abe0e1e84', '2016-07-21 13:02:56', 0, 0, 0, 0),
(38, 'Shadi', 'cmtsheikeshdi@gmail.com', '1', 'Yes', '5790abd3ddfbf', '2016-07-21 13:02:43', 0, 0, 0, 0),
(37, 'Milton', 'Milton@gmail.com', '1', 'Yes', '5790abb2548cd', '2016-07-21 13:02:10', 0, 0, 0, 0),
(40, 'Shadi', 'cmtsheikeshdi@gmail.com', 'a', 'Yes', '5790ac0b1c23b', '2016-07-21 13:03:39', 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `valide`
--

CREATE TABLE IF NOT EXISTS `valide` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `u_id` varchar(255) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `valide`
--

INSERT INTO `valide` (`id`, `title`, `u_id`) VALUES
(11, 'Sony', '57820cd6337b1'),
(9, 'Sheik', '57820be1c0587'),
(8, 'Nokia', '57820abb7315b'),
(26, 'Dell', '578760a11fb19'),
(27, 'LG', '578760a72ef89'),
(28, 'Symphony', '578760acb8561'),
(29, 'Samsung', '578760b38a919');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `books`
--
ALTER TABLE `books`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `check`
--
ALTER TABLE `check`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `exam`
--
ALTER TABLE `exam`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gender`
--
ALTER TABLE `gender`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mobile`
--
ALTER TABLE `mobile`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mobile_2`
--
ALTER TABLE `mobile_2`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pdo`
--
ALTER TABLE `pdo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `valide`
--
ALTER TABLE `valide`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `books`
--
ALTER TABLE `books`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `check`
--
ALTER TABLE `check`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `exam`
--
ALTER TABLE `exam`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `gender`
--
ALTER TABLE `gender`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `mobile`
--
ALTER TABLE `mobile`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `mobile_2`
--
ALTER TABLE `mobile_2`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `pdo`
--
ALTER TABLE `pdo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `student`
--
ALTER TABLE `student`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=41;
--
-- AUTO_INCREMENT for table `valide`
--
ALTER TABLE `valide`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=31;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
