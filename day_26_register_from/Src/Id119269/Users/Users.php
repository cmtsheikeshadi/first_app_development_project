<?php
namespace App\Id119269\Users;
use PDO;

class Users{
    public $id = "";
    public $title = "";
    public $name = "";
    public $email = "";
    public $password = "";
    public $c_password = "";
    public $terms = "";
    public $conn = "";

    public function __construct() {
        session_start();
        $this->conn = new PDO('mysql:dbname=id119269;host=localhost', 'root', '');
    }

    public function prepare($data=""){
        if(!empty($data['id'])){
            $this->id = $data['id'];
        }
        if(!empty($data['name']) && strlen($data['name'])>3 && strlen($data['name'])<10){
           $this->name = $data['name'];
        }  else {
            $_SESSION['Message'] = "<p style='color:red;'>Name <3 and 10>";
            header('location:creat.php');
        }
        if(!empty($data['email'])){
            $this->email = $data['email'];
        }
        if(!empty($data['password'])){
            $this->password = $data['password'];
        }else{
           $_SESSION['password'] = "<p style='color:red;'>Enter Password";
            header('location:creat.php'); 
        }
        if(!empty($data['c_password'])){
            $this->c_password = $data['c_password'];
        }else{
           $_SESSION['password'] = "<p style='color:red;'>Enter c_Password";
            header('location:creat.php'); 
        }
        
        if(!empty($data['terms'])){
            $this->terms = $data['terms'];
        } else{
           $_SESSION['password'] = "<p style='color:red;'>Get Terms Condition";
            header('location:creat.php'); 
        }
        return $this;
       // print_r($data);
    }
    
    public function store(){
        if(!empty($this->name) && !empty($this->password) && $this->password==$this->c_password && !empty($this->terms)){
        try {
            $time = date('Y-m-d H:i:s');
            $query = "INSERT INTO `users`(user_name, email, password, terms, unique_id , is_active)
                VALUES(:user_name, :email, :password, :terms, :unique_id, :is_active)";
            
            $result = $this->conn->prepare($query);
            $result->execute(array(
                "user_name" => $this->name,
                "email" => $this->email,
                "password" => $this->password,
                "terms" => $this->terms,
                "unique_id" => uniqid(),
                "is_active" => date('Y-m-d H:i:s'),
                 
            ));
            
            $_SESSION['Message'] = 'Data add Successfully';
            header('location: index.php');
        } catch (PDOException $e) {
            echo "Error: " . $e->getMessage();
        }
        }else{
            if(!empty($this->terms)){
            $_SESSION['password'] = "<p style='color:red;'>Password Not match";
            header('location:creat.php');
            }else{
                $_SESSION['password'] = "<p style='color:red;'>Get Terms Condition";
            header('location:creat.php');
            }
        }  
            
       
    }
    
    public function index(){
        try{
            $query = "SELECT * FROM `users`";
            $result = $this->conn->query($query) or die("Index query not ok");
            while ($row = $result->fetch(PDO::FETCH_ASSOC)){
                    $data[] = $row;
                }
            } catch (PDOException $e){
            echo"Error: " .$e->getMassage();
            }
            return $data;
    }
    
    public function show(){
        try{
            $query = "SELECT * FROM `pdo` WHERE id=$this->id";
            $result = $this->conn->query($query);
            $row = $result->fetch(PDO::FETCH_ASSOC);
            return $row; 
        }catch (PDOException $e){
            echo"Error: " .$e->getMassage();
        }
    }
    
    public function delete(){
        try{
            $query = "DELETE FROM `id119269`.`pdo` WHERE `pdo`.`id` = $this->id";
            $this->conn->query($query);
            $_SESSION['Message'] = "Data has been Delete";
            header('location:index.php');
        }catch (PDOException $e){
            echo"Error: " .$e->getMassage();
        }
    }
    
    public function update(){
        $query = "UPDATE `id119269`.`pdo` SET `c_name` = '".$this->c_name."', `c_color` = '".$this->c_color."' WHERE `pdo`.`id` = $this->id;";
        $this->conn->query($query);
        $_SESSION['Message'] = "Data has been Update";
        header('location:index.php');
    }   
    
}


?>
