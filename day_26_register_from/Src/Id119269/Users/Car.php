<?php
namespace App\Id119269\Car;
use PDO;

class Car{
    public $id = "";
    public $title = "";
    public $s_name = "";
    public $semester = "";
    public $check = "";
    public $conn = "";

    public function __construct() {
        session_start();
        $this->conn = new PDO('mysql:dbname=id119269;host=localhost', 'root', '');
    }

    public function prepare($data=""){
        if(!empty($data['id'])){
            $this->id = $data['id'];
        }
        if(!empty($data['s_name'])){
            $this->s_name = $data['s_name'];
        }
        if(!empty($data['semester'])){
            $this->semester = $data['semester'];
        }
        if(!empty($data['check'])){
            $this->check = $data['check'];
        }
        return $this;
       // print_r($data);
    }
    
    public function store(){
        try {
          $query = "INSERT INTO `pdo`(name, s, c)
                VALUES(:name, :s, :c)";
            
            $result = $this->conn->prepare($query);
            $result->execute(array(
                "name" => '$this->s_name',
                "s" => '$this->semester',
                "c" => '$this->check',
            ));
            
            $_SESSION['Message'] = 'Data add Successfully';
            header('location: index.php');
        } catch (PDOException $e) {
            echo "Error: " . $e->getMessage();
        }
    }
    
    public function index(){
        try{
            $query = "SELECT * FROM `pdo`";
            $result = $this->conn->query($query) or die("Index query not ok");
            while ($row = $result->fetch(PDO::FETCH_ASSOC)){
                    $data[] = $row;
                }
            } catch (PDOException $e){
            echo"Error: " .$e->getMassage();
            }
            return $data;
    }
    
    public function show(){
        try{
            $query = "SELECT * FROM `pdo` WHERE id=$this->id";
            $result = $this->conn->query($query);
            $row = $result->fetch(PDO::FETCH_ASSOC);
            return $row; 
        }catch (PDOException $e){
            echo"Error: " .$e->getMassage();
        }
    }
    
    public function delete(){
        try{
            $query = "DELETE FROM `id119269`.`pdo` WHERE `pdo`.`id` = $this->id";
            $this->conn->query($query);
            $_SESSION['Message'] = "Data has been Delete";
            header('location:index.php');
        }catch (PDOException $e){
            echo"Error: " .$e->getMassage();
        }
    }
    
    public function update(){
        $query = "UPDATE `id119269`.`pdo` SET `c_name` = '".$this->c_name."', `c_color` = '".$this->c_color."' WHERE `pdo`.`id` = $this->id;";
        $this->conn->query($query);
        $_SESSION['Message'] = "Data has been Update";
        header('location:index.php');
    }   
    
}


?>
