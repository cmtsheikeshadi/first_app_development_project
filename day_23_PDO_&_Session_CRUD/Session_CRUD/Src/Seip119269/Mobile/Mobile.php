<?php

namespace App\Seip119269\Mobile;

use PDO;

class Mobile {

    public $id = "";
    public $title = "";
    public $data = "";
    public $name = "";
    public $dbusername = "root";
    public $dbpassword = "";
    public $conn = '';

    public function __construct() {
        session_start();
        $this->conn = new PDO('mysql:dbname=id119269;host=localhost', 'root', '');
   }

    public function prepare($data = "") {

        if (!empty($data['id'])) {
            $this->id = $data['id'];
        }

        if (!empty($data['model'])) {
            $this->title = $data['model'];
        } else {
            if (isset($data['model'])) {
                $_SESSION['Model'] = "Fill in the gap Model";
                header('location:create.php');
            }
        }

        if (!empty($data['name'])) {
            $this->name = $data['name'];
        } else {
            if (isset($data['name'])) {
                $_SESSION['Name'] = "Fill in the gap Name";
                header('location:create.php');
            }
        }
        return $this;
    }

    public function store() {

        try {
            $query = "INSERT INTO valide(title, u_id)
                VALUES(:title, :u_id)";

            $result = $this->conn->prepare($query);
            $result->execute(array(
                "title" => $this->title,
                "u_id" => uniqid(),
            ));
            
            $_SESSION['Message'] = 'Data add Successfully';
            header('location: index.php');
        } catch (PDOException $e) {
            echo "Error: " . $e->getMessage();
        }
    }

    public function index() {
       try{
            $query = "SELECT * FROM `valide`";
            $result = $this->conn->query($query) or die ('Unable to index query');
            While($row = $result->fetch(PDO::FETCH_ASSOC)){
                $this->data[] = $row;
            }
        } catch (Exception $ex) {
            echo "Error: " . $e->getMessage();
        }
        return $this->data;
        return $this;
    }

    public function show() {
         
        $query = "SELECT * FROM `valide` WHERE `u_id`= " . "\"$this->id\"";
        $result = $this->conn->query($query);
        $row = $result->fetch(PDO::FETCH_ASSOC);
        return $row; 
    }

    public function delete() {
        try{
             $query = "DELETE FROM `id119269`.`valide` WHERE `valide`.`u_id` = " . "\"$this->id\"";
             $this->conn->query($query);
                $_SESSION['Message'] = 'Data add Successfully Delete';
                header('location: index.php');
        } catch (Exception $ex) {
            echo "Error: " . $e->getMessage(); 
        }
        
        
    }

    public function update() {
        try {
            $query = "UPDATE `id119269`.`valide` SET `title` = '$this->title' WHERE `valide`.`u_id` = " . "\"$this->id\"";
            $this->conn->query($query);
        } catch (Exception $ex) {
            
        }
        $_SESSION['Message'] = 'Data add Successfully Update';
        header('location: index.php');
    }
}

?>