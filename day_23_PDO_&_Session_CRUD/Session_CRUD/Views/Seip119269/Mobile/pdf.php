<?php

include_once ('../../../vendor/mpdf/mpdf/mpdf.php');
include_once ('../../../vendor/autoload.php');

use App\Seip119269\Mobile\Mobile;
$obj = new Mobile();
$alldata = $obj->index();

$trs ='';
$serial = 0;

foreach ($alldata as $data):
    $serial++;
    $trs.="<tr>";
    $trs.="<td>".$serial."</td>";
    $trs.="<td>".$data['id']."</td>";
    $trs.="<td>".$data['title']."</td>";
    $trs.="</tr>";
endforeach;

$html = <<<EOD
<html>
   <head>
        <title>pdf</title>
   </head>
   <body>
      <h1> List of Mobile </h1>
        <table border="1">
            <thead>
                <tr>
                    <th>SI.</th>
                    <th>ID</th>
                    <th>Title</th>
                </tr>
            </thead>
        
        <tbody>
            $trs;
        </tbody>
   </table>
   </body>
</html>
EOD;

$mpdf = new mPDF();
$mpdf->writeHTML($html);
$mpdf->Output();
exit();

?>