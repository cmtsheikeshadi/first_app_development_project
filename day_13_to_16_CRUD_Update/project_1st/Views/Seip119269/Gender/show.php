<?php
include ('../../../Src/Seip119269/Gender/Gender.php');

$obj = new Gender();
$data = $obj->prepare($_GET)->show();
?>

<table border="1">
    <tr>
        <th>ID</th>
        <th>Title</th>
        <th>Gender</th>
    </tr>
    <tr>
        <td><?php echo $data['id'];?></td>
        <td><?php echo $data['title'];?></td>
        <td><?php echo $data['gender'];?></td>
    </tr>
</table>