<?php
namespace App\Id119269\Mobile;
use PDO;

class Mobile{
    public $id = "";
    public $title = "";
    public $name = "";
    public $mobile = "";
    public $time = "";
    public $conn = "";

    public function __construct() {
        session_start();
        $this->conn = new PDO('mysql:dbname=id119269;host=localhost', 'root', '');
    }

    public function prepare($data=""){
        if(!empty($data['id'])){
            $this->id = $data['id'];
        }
        if(!empty($data['name'])){
            $this->name = $data['name'];
        }
        if(!empty($data['mobile'])){
            $this->mobile = $data['mobile'];
        }
        
        return $this;
       // print_r($data);
    }
    
    public function store(){
        try {
          $query = "INSERT INTO `mobile_2`(name, mobile, time)
                VALUES(:name, :mobile, :time)";
            
            $result = $this->conn->prepare($query);
            $result->execute(array(
                "name" => $this->name,
                "mobile" => $this->mobile,
                "time" => date("h:i:sa"),
            ));
            
            $_SESSION['Message'] = 'Data add Successfully';
            header('location: index.php');
        } catch (PDOException $e) {
            echo "Error: " . $e->getMessage();
        }
    }
    
    public function index(){
        try{
            $query = "SELECT * FROM `mobile_2` WHERE `is_delete`='0'";
            $result = $this->conn->query($query) or die("Index query not ok");
            while ($row = $result->fetch(PDO::FETCH_ASSOC)){
                    $data[] = $row;
                }
            } catch (PDOException $e){
            echo"Error: " .$e->getMassage();
            }
            return $data;
    }
    
    public function show(){
        try{
            $query = "SELECT * FROM `mobile_2` WHERE id=$this->id";
            $result = $this->conn->query($query);
            $row = $result->fetch(PDO::FETCH_ASSOC);
            return $row; 
        }catch (PDOException $e){
            echo"Error: " .$e->getMassage();
        }
    }
    
    public function delete(){
        try{
            $query = "DELETE FROM `id119269`.`mobile_2` WHERE `mobile_2`.`id` = $this->id";
            $this->conn->query($query);
            $_SESSION['Message'] = "Data has been Delete";
            header('location:restore_show.php');
        }catch (PDOException $e){
            echo"Error: " .$e->getMassage();
        }
    }
    
    public function update(){
        $query = "UPDATE `id119269`.`mobile_2` SET `name` = '$this->name', `mobile` = '$this->mobile' WHERE `mobile_2`.`id` = $this->id;";
        $this->conn->query($query);
        $_SESSION['Message'] = "Data has been Update";
        header('location:index.php');
    }   
    
    public function trashed(){
        $time = date("h:i:sa");
        $query = "UPDATE `id119269`.`mobile_2` SET `is_delete` = '1', `time` = '$time' WHERE `mobile_2`.`id` = $this->id;";
        $this->conn->query($query);
        $_SESSION['Message'] = "Data has been Trashed";
        header('location:index.php');
    }
    
    public function resore(){
        $query = "UPDATE `id119269`.`mobile_2` SET `is_delete` = '0' WHERE `mobile_2`.`id` = $this->id;";
        $this->conn->query($query);
        $_SESSION['Message'] = "Data has been Trashed";
        header('location:index.php');    
    }
    
    public function trashed_show(){
         try{
            $query = "SELECT * FROM `mobile_2` WHERE `is_delete`='1' ORDER BY `time` DESC ";
            $result = $this->conn->query($query) or die("Index query not ok");
            while ($row = $result->fetch(PDO::FETCH_ASSOC)){
                    $data[] = $row;
                }
            } catch (PDOException $e){
            echo"Error: " .$e->getMassage();
            }
            return $data;
    }
    
}


?>
