<?php include('head.php');?>

<table id="tableData" class="table table-bordered table-striped">
   <thead>
    <tr>
        <th>id</th>
        <th> Name</th>
        <th>Mobile</th>
        <th colspan="3">Action</th>
    </tr>
  </thead>
  <tbody>
  
<?php
        include_once ('../../../vendor/autoload.php');
        use App\Id119269\Mobile\Mobile;
        $obj = new Mobile();
        $datas = $obj->index();
       
    if(isset($_SESSION['Message']) && !empty($_SESSION['Message'])){
        echo $_SESSION['Message'];
        unset($_SESSION['Message']);
    }
?>
    
 <?php foreach ($datas as $data): ?>   
    <tr>
        <td><?php echo $data['id'];?></td>
        <td><?php echo $data['name'];?></td>
        <td><?php echo $data['mobile'];?></td>
        <td> 
            <a href="show.php?id=<?php echo $data['id'];?>">View</a>
            <a href="edit.php?id=<?php echo $data['id'];?>" onclick="return confirm('Are you sure you want to Edit this item?');">Edit</a>
            <a href="trashed.php?id=<?php echo $data['id'];?>" onclick="return confirm('Are you sure you want to Trashed this item?');">Delete</a>
       </td>
    </tr>
 <?php endforeach;?>

      
  </tbody>
</table>

<?php include('footer.php');?>     
        