<?php include('head.php');?>

<table id="tableData" class="table table-bordered table-striped">
    <tr>
        <th>id</th>
        <th>Name</th>
        <th>Mobile</th>
        <th colspan="2">Action</th>
    </tr>
   
<?php
        include_once ('../../../vendor/autoload.php');
        use App\Id119269\Mobile\Mobile;
        $obj = new Mobile();
        $datas = $obj->trashed_show();
       
    if(isset($_SESSION['Message']) && !empty($_SESSION['Message'])){
        echo $_SESSION['Message'];
        unset($_SESSION['Message']);
    }
?>
    
 <?php foreach ($datas as $data): ?>   
    <tr>
        <td><?php echo $data['id'];?></td>
        <td><?php echo $data['name'];?></td>
        <td><?php echo $data['mobile'];?></td>
        <td> 
            <a href="restore.php?id=<?php echo $data['id'];?>" onclick="return confirm('Are you sure you want to Restore this item?');">Restore</a>
            <a href="Delete.php?id=<?php echo $data['id'];?>" onclick="return confirm('Are you sure you want to Delete this item?');">Delete</a>
            
       </td>
    </tr>
 <?php endforeach;?>

</table>

<?php include('footer.php');?>