<?php
include ('../../../vendor/autoload.php');

use App\Seip119269\Mobile\Mobile;

$obj = new Mobile();
$data = $obj->prepare($_GET)->show();

if(isset($data['u_id']) && $data['u_id'] == $_GET['id'] && !empty($data)){
   $obj->prepare($_GET)->delete();
}  else {
    $_SESSION['Massage'] = "Page Not Found";
    header('location:404.php');
}
