<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInitf669be04bb4d47e1180025e308343179
{
    public static $prefixLengthsPsr4 = array (
        'A' => 
        array (
            'App\\' => 4,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'App\\' => 
        array (
            0 => __DIR__ . '/../..' . '/Src',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInitf669be04bb4d47e1180025e308343179::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInitf669be04bb4d47e1180025e308343179::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
