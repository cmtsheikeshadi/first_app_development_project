<?php
namespace App\Id119269\Student;
use PDO;

class Student{
    public $conn = '';
    public $id = '';
    public $title = '';
    public $name = '';
    public $semester = '';
    public $webyer = '';
    public $fee = '';
    public $ans = '';
    public $dis = '';
    public $total = '';
    

    public function __construct() {
        session_start();
        $this->conn = new PDO('mysql:dbname=id119269;host=localhost', 'root', '');
    }

    public function prepare($data=""){
        if(!empty($data['id'])){
            $this->id = $data['id'];
        }
        if(!empty($data['name'])){
            $this->name = $data['name'];
        }
        if(!empty($data['semester'])){
            $this->semester = $data['semester'];
        }
        if(!empty($data['webyer'])){
            if($data['webyer'] && $data['semester']=="First_Semester"){
                $this->webyer = 'Yes';
                $this->fee = "10000";
                $this->par = "10";
                $this->dis = $this->fee*$this->par/100;
                $this->total = $this->fee-$this->ans;
            }  
            if($data['webyer'] && $data['semester']=="Second_Semester"){
                $this->webyer = 'Yes';
                $this->fee = "15000";
                $this->par = "7";
                $this->dis = $this->fee*$this->par/100;
                $this->total = $this->fee-$this->ans;
            }
            if($data['webyer'] && $data['semester']=="Third_Semester"){
                $this->webyer = 'Yes';
                $this->fee = "20000";
                $this->par = "5";
                $this->dis = $this->fee*$this->par/100;
                $this->total = $this->fee-$this->ans;
            }
        }else {
                $this->webyer = 'NO';
            }
        return $this;
       // print_r($data);
    }
    
    public function store(){
        try {
            $query = "INSERT INTO `id119269`.`pdo` (`name`, `semester`, `webyer`, `fee`, `dis`, `total`)
                VALUES(:n, :s, :w, :f, :d, :t)";

            $result = $this->conn->prepare($query);
            $result->execute(array(
                "n" => $this->name,
                "s" => $this->semester,
                "w" => $this->webyer,
                "f" => $this->fee,
                "d" => $this->ans,
                "t" => $this->total,
            ));
            
            $_SESSION['Message'] = 'Data add Successfully';
            header('location: index.php');
        } catch (PDOException $e) {
            echo "Error: " . $e->getMessage();
        }
    }
    
    public function index(){
        try{
            $query = "SELECT * FROM `pdo`";
            $result = $this->conn->query($query) or die("Index query not ok");
            while ($row = $result->fetch(PDO::FETCH_ASSOC)){
                    $data[] = $row;
                }
            } catch (PDOException $e){
            echo"Error: " .$e->getMassage();
            }
            if(isset($data)){return $data;}
    }
    
    public function show(){
        try{
            $query = "SELECT * FROM `pdo` WHERE id=$this->id";
            $result = $this->conn->query($query);
            $row = $result->fetch(PDO::FETCH_ASSOC);
            return $row; 
        }catch (PDOException $e){
            echo"Error: " .$e->getMassage();
        }
    }
    
    public function delete(){
        try{
            $query = "DELETE FROM `id119269`.`pdo` WHERE `pdo`.`id` = $this->id";
            $this->conn->query($query);
            $_SESSION['Message'] = "Data has been Delete";
            header('location:index.php');
        }catch (PDOException $e){
            echo"Error: " .$e->getMassage();
        }
    }
    
    public function update(){
        $query = "UPDATE `id119269`.`pdo` SET `name` = '$this->name', `semester` = '$this->semester', `webyer` = '$this->webyer', `fee` = '$this->fee', `dis` = '$this->dis', `total` = '$this->total' WHERE `pdo`.`id` = $this->id;";
        $this->conn->query($query);
        $_SESSION['Message'] = "Data has been Update";
        header('location:index.php');
    }   
    
}


?>
