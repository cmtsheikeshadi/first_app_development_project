<?php


$a=array("a"=>"Volvo","b"=>"BMW","c"=>"Toyota");

echo '<pre>';
print_r($a);
echo "</pre>";

echo '<br/>';

echo '<pre>';
print_r(array_reverse($a));
echo '</pre>';

$a=array("Volvo","XC90",array("BMW","Toyota"));

$reverse=array_reverse($a);
echo '<pre>';
print_r($reverse);
echo '</pre>';


$preserve=array_reverse($a,true);
echo '<pre>';
print_r($preserve);
echo '</pre>';


?>